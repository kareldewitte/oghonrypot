# oghonrypot

This script is a jquery script that randomizes names of a form in order to avoid being parsed by a bot.
In addition it also adds additionnal honeypot fields that should be hidden via css not display: none in style.
Once the visitor clicks submit it will will check those fields, if they are empty it will restore the names and submit the form.

It is intended to be the less invasive as possible. And should be compatible with the pre-existing backend servlet.

