(function ($) {
  $.fn.honylize = function () {

    function rand() {
      return Math.random().toString(36).replace('0.', '');
    }

    return this.each(function () {

      var $form = $(this);
      var $formInputs = $('select, input, textarea', $form);
      var inputfields = {};

      $formInputs.each(function () {

        var $formInput = $(this);
        if ($formInput.attr('type') !== 'hidden') {
          var rr = rand();
          inputfields[rr] = $formInput.attr('name');
          $formInput.attr('name', rr);
        }
      });

      $form.data('ipsf1234', inputfields);

      // Add honeypot fields
      $('<input />').attr('name', 'hp-url').attr('class', 'no-disp').attr('value', '').appendTo($form);
      $('<input />').attr('name', 'hp-name').attr('class', 'no-disp').attr('value', '').appendTo($form);
      $('<input />').attr('name', 'hp-text').attr('class', 'no-disp').attr('value', '').appendTo($form);

      $form.on('submit', function (event) {
        var hny = ['hp-url', 'hp-name', 'hp-text'];
        var result = {};
        var corresp = $(this).data('ipsf1234');
        var data = $(this).serializeArray();
        var shouldSubmit = true;
        $.each(data, function () {
          result[this.name] = this.value;
        });

        $.each(hny, function (k, v) {
          if (result[v].length > 0) {
            shouldSubmit = false;
            event.preventDefault();
          }
        });

        $.each(corresp, function (k, v) {
          var $el = $form.find($('[name=' + k + ']'));
          $el.attr('name', v);
        });

        $('input.no-disp', $form).remove();

        return shouldSubmit;
      });
    });
  };
})(jQuery);
